build:
	stack build

debug: build
	stack exec sudo debug

ghci-fingerd:
	stack ghci --main-is fingerd:exe:fingerd

ghci-debug:
	stack ghci --main-is fingerd:exe:debug

run: build
	stack exec sudo fingerd

# I was able to get it working, but only by
# running `stack exec which fingerd` and then
# running that with sudo in front of it.
# It _seems_ like the sudo invocation under `stack exec`
# is jumping the gun or something.
# I would investigate further, but...internet access.

# but
# julie@doriangray:~/girl/fingerd$ stack exec which fingerd
# /home/julie/girl/fingerd/.stack-work/install/x86_64-linux/lts-5.15/7.10.3/bin/fingerd
# julie@doriangray:~/girl/fingerd$ sudo stack exec which fingerd
# You are not the owner of '/home/julie/.stack/'. Aborting to protect file permissions.
# Retry with '--allow-different-user' to disable this precaution.
# julie@doriangray:~/girl/fingerd$ stack exec sudo fingerd
# sudo: fingerd: command not found
# julie@doriangray:~/girl/fingerd$ stack exec sudo which fingerd
# julie@doriangray:~/girl/fingerd$ 

# so idk but this doesn't seem helpful